#ifndef IMAGEOVERLAY_H
#define IMAGEOVERLAY_H

#include <QGraphicsObject>
#include <QtGui>

class ImageOverlay : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit ImageOverlay(QGraphicsItem *parent = 0, QGraphicsScene *_parentScene = 0,
                          int _width = 100, int _height = 100);

    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

private:
    int width;
    int height;
    QGraphicsScene* parentScene;
    
signals:
    
public slots:
    
};

#endif // IMAGEOVERLAY_H
