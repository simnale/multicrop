#include <QtGui>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "imageoverlay.h"
#include "overlaydialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    rawImage(new QPixmap),
    scene(new QGraphicsScene(this)),
    graphicsPixmap(new QGraphicsPixmapItem)
{
    ui->setupUi(this);
    setWindowTitle("multicrop");

    scene->addText("Please load an image.");
    ui->view->setScene(scene);
    ui->view->setCacheMode(QGraphicsView::CacheNone);

    connect(ui->openButton, SIGNAL(clicked()), this, SLOT(loadImage()));
    connect(ui->inButton, SIGNAL(clicked()), this, SLOT(zoomIn()));
    connect(ui->outButton, SIGNAL(clicked()), this, SLOT(zoomOut()));
    connect(ui->overlayButton, SIGNAL(clicked()), this, SLOT(addOverlay()));
    connect(ui->clearButton, SIGNAL(clicked()), this, SLOT(clearAll()));
    connect(ui->createButton, SIGNAL(clicked()), this, SLOT(createImages()));
}

MainWindow::~MainWindow()
{
    delete rawImage;
    delete graphicsPixmap;
    delete ui;
}

void MainWindow::clearAll()
{
    foreach (QGraphicsItem* item, scene->items())
    {
        scene->removeItem(item);
    }
    scene->addItem(graphicsPixmap);
}

void MainWindow::createImages()
{
    auto list = scene->items();

    if (list.length() == 1)
    {
        QMessageBox::warning(this, tr("Error"), tr("You need to load in image."), QMessageBox::Ok);
        return;
    }

    QString fullPath = QFileDialog::getSaveFileName(this, tr("Save Images"), "");

    if (fullPath.isEmpty()) return;

    QString path = fullPath.split(".").first();
    QString ending = fullPath.split(".").last();

    if (path == ending)
    {
        QMessageBox::warning(this, tr("Error"), tr("You forgot to specify a filetype."), QMessageBox::Ok);
        return;
    }

    qDebug() << path << ending;

    for (int i = 0; i < list.count()-1; ++i) /* original picture is last element, therefor -1 */
    {
        QRectF rectParent = list[i]->mapRectToParent(list[i]->boundingRect());
        QImage image(filename);
        QImage copy = image.copy(rectParent.x(), rectParent.y(),
                                 rectParent.width(), rectParent.height());
        copy.save(path+QString::number(i)+QString(".")+ending);
    }
}

void MainWindow::loadImage()
{
    foreach (QGraphicsItem* item, scene->items())
    {
        scene->removeItem(item);
    }

    filename = QFileDialog::getOpenFileName(this, tr("Open image"), "", tr("Files (*.*)"));
    rawImage->load(filename);
    graphicsPixmap->setPixmap(*rawImage);
    scene->addItem(graphicsPixmap);
    ui->view->setSceneRect(rawImage->rect());
}

void MainWindow::zoomIn()
{
    ui->view->scale(1.25,1.25);
}

void MainWindow::zoomOut()
{
    ui->view->scale(0.75, 0.75);
}

void MainWindow::addOverlay()
{
    if (rawImage->isNull())
    {
        return;
    }

    OverlayDialog dialog(this);
    int x = 0;
    int y = 0;
    if (dialog.exec())
    {
        x = dialog.width();
        y = dialog.height();
    }

    if (x != 0 && y != 0)
    {
        new ImageOverlay(graphicsPixmap, scene, x, y);
    }
}
