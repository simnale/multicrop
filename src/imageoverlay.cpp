#include <QtGui>
#include "imageoverlay.h"

ImageOverlay::ImageOverlay(QGraphicsItem *parent,
                           QGraphicsScene *_parentScene,
                           int _width, int _height) :
    QGraphicsObject(parent),
    parentScene(_parentScene),
    width(_width),
    height(_height)
{
    setFlag(QGraphicsItem::ItemIsSelectable);
    setFlag(QGraphicsItem::ItemIsMovable);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges);
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges);
}

void ImageOverlay::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    parentScene->removeItem(this);
}

QRectF ImageOverlay::boundingRect() const
{
    return QRectF(0, 0, width, height);
}

void ImageOverlay::paint(QPainter *painter, 
			 const QStyleOptionGraphicsItem *option,
			 QWidget *widget)
{
    painter->setRenderHint(QPainter::HighQualityAntialiasing, true);
    painter->setBrush(QColor(200, 0, 0, 150));
    painter->drawRect(0, 0, width, height);
}
