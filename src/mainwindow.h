#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <QLabel>
#include <QGraphicsScene>
#include <QtGui>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void deleteFromScene(QGraphicsItem* item);
    
private:
    Ui::MainWindow *ui;
    QPixmap *rawImage;
    QGraphicsScene *scene;
    QGraphicsPixmapItem *graphicsPixmap;
    QString filename;

private slots:
    void loadImage();
    void zoomIn();
    void zoomOut();
    void addOverlay();
    void clearAll();
    void createImages();
};

#endif // MAINWINDOW_H
